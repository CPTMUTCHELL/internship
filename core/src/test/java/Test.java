import org.junit.Assert;
import org.junit.Before;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Test {
    private Service service;
    private List<Person> listWithMatch;
    private List<Person> list;
    private Person person;
    @Before
    public void generateListWithoutMatch(){
        service = new ServiceImpl();
        list=new ArrayList<>();
        listWithMatch=new ArrayList<>();
        person=new Person("Alex","Shmykov","Dm",23, Sex.MALE, LocalDate.now().minusYears(23));
        list.add(person);
        list.add(new Person("Alex","mykov","Dm",23, Sex.MALE,LocalDate.now().minusYears(23)));
        list.add(new Person("Bob","cdsv","Dm",19, Sex.FEMALE,LocalDate.now().minusYears(14)));
        list.add(new Person("Alex","Amykov","Dm",3, Sex.MALE,LocalDate.now().minusYears(10)));

        listWithMatch.add(new Person("Alex","Shmykov","Dm",23, Sex.MALE, LocalDate.now().minusYears(23)));
        listWithMatch.add(new Person("Alex","Shmykov","Dm",23, Sex.MALE,LocalDate.now().minusYears(23)));
        listWithMatch.add(new Person("Bob","cdsv","Dm",19, Sex.FEMALE,LocalDate.now().minusYears(14)));
        listWithMatch.add(new Person("Alex","Amykov","Dm",50,Sex.MALE,LocalDate.now().minusYears(10)));
    }
    @org.junit.Test
    public void isMatchTest(){
        Assert.assertTrue(service.hasMatch(listWithMatch));
        Assert.assertFalse(service.hasMatch(list));
    }
    @org.junit.Test
    public void filterHasTwoItemsTest (){
        List<Person> filteredList = service.filter(list, 25, "s");
        Assert.assertEquals(1,filteredList.size());
        Assert.assertTrue(filteredList.contains(person));

    }
}
