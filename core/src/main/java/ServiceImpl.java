import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ServiceImpl implements Service{

    @Override
     public boolean hasMatch(List<Person> list){
        Set<Person> set = new HashSet<>(list);
        return set.size()!=list.size();
    }
    @Override
    public List<Person> filter(List<Person> list, int maxAge, String lastNameLetter){
        return list.stream()
                .filter(p->p.getAge()<=maxAge && p.getLastname().toLowerCase().startsWith(lastNameLetter.toLowerCase()))
                .collect(Collectors.toList());

    }

}
