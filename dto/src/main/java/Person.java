import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.Objects;

public class Person {
   private String firstname;
   private String lastname;
   private String middlename;
   private int age;
   private Sex sex;
   private LocalDate birthDate;

   public Person(String firstname, String lastname, String middlename, int age, Sex sex, LocalDate birthDate) {
      this.firstname = firstname;
      this.lastname = lastname;
      this.middlename = middlename;
      this.age = age;
      this.sex = sex;
      this.birthDate = birthDate;
   }


   public String getFirstname() {
      return firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public String getMiddlename() {
      return middlename;
   }

   public int getAge() {
      return age;
   }

   public Sex getSex() {
      return sex;
   }

   public LocalDate getBirthDate() {
      return birthDate;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Person person = (Person) o;
      return age == person.age && Objects.equals(firstname, person.firstname) && Objects.equals(lastname, person.lastname) && Objects.equals(middlename, person.middlename) && sex == person.sex && Objects.equals(birthDate, person.birthDate);
   }

   @Override
   public int hashCode() {
      return Objects.hash(firstname, lastname, middlename, age, sex, birthDate);
   }

   @Override
   public String toString() {
      return "Person{" +
              "firstname='" + firstname + '\'' +
              ", lastname='" + lastname + '\'' +
              ", middlename='" + middlename + '\'' +
              ", age=" + age +
              ", sex=" + sex +
              ", birthDate=" + birthDate +
              '}';
   }
}
