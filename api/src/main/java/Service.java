import java.util.List;

public interface Service {
    boolean hasMatch(List<Person> list);
    List<Person> filter(List<Person> list,int maxAge, String lastNameLetter);

}
